resource "random_string" "password" {
  length  = 16
  special = false
}

resource "aws_ssm_parameter" "DB_endpoint" {
  name        = "/${var.db_tablename}/ENDPOINT"
  description = "DB Endpoint"
  type        = "SecureString"
  key_id      = "alias/aws/ssm"
  value       = aws_db_instance.myrds.endpoint
}

resource "aws_ssm_parameter" "DB_name" {
  name        = "/${var.db_tablename}/DBNAME"
  description = "RDS name"
  type        = "SecureString"
  key_id      = "alias/aws/ssm"
  value       = var.db_tablename
}


resource "aws_ssm_parameter" "db_user" {
  name        = "/${var.db_tablename}/USER"
  description = "RDS User"
  type        = "SecureString"
  key_id      = "alias/aws/ssm"
  value       = var.db_username
}

resource "aws_ssm_parameter" "rds_db_password" {
  name        = "/${var.db_tablename}/PASSWORD"
  description = "RDS Password"
  type        = "SecureString"
  key_id      = "alias/aws/ssm"
  value       = random_string.password.result

}
