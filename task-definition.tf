
# "arn:aws:iam::702713127235:role/taskDefinitionFinalChallenge"
# "arn:aws:iam::702713127235:role/ecsTaskExecutionRole"
resource "aws_ecs_task_definition" "tf-wordpress" {
  family                   = "finalTaskWordPress"
  requires_compatibilities = ["FARGATE"]
  execution_role_arn       = aws_iam_role.task_def.arn
  task_role_arn            = aws_iam_role.task_def.arn
  network_mode             = "awsvpc"
  cpu                      = 1024
  memory                   = 2048

  container_definitions = jsonencode([
    {
      name      = "wordpress"
      image     = "wordpress:latest"
      cpu       = 256
      memory    = 512
      essential = true
      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
        }
      ]
      # "secrets" : [
      #   {
      #     "name" : "WORDPRESS_DB_HOST",
      #     "valueFrom" : "${aws_ssm_parameter.DB_endpoint.arn}"
      #   },
      #   {
      #     "name" : "WORDPRESS_DB_USER",
      #     "valueFrom" : "${aws_ssm_parameter.db_user.arn}"
      #   },
      #   {
      #     "name" : "WORDPRESS_DB_PASSWORD",
      #     "valueFrom" : "${aws_ssm_parameter.rds_db_password.arn}"
      #   },
      #   {
      #     "name" : "WORDPRESS_DB_NAME",
      #     "valueFrom" : "${aws_ssm_parameter.DB_name.arn}"
      #   }
      # ],
      # "mountPoints" : [
      #   {
      #     "ContainerPath" : "/var/www/html/",
      #     "SourceVolume" : "service-storage"
      #   }
      # ]
    }
  ])
  volume {
    name = "service-storage"
    efs_volume_configuration {
      file_system_id     = aws_efs_file_system.efs-ecs.id
      root_directory     = "/"
      transit_encryption = "ENABLED"
      authorization_config {
        access_point_id = aws_efs_access_point.ap-ecs.id

      }
    }
  }
}

