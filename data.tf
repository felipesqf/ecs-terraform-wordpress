data "aws_kms_key" "kms_efs" {
  key_id = "alias/aws/elasticfilesystem"
}

data "aws_acm_certificate" "felipesqf" {
  domain   = "*.felipesqf.com"
  statuses = ["ISSUED"]
}

data "aws_route53_zone" "felipesqf" {
  name = "felipesqf.com"
}

data "aws_vpc" "vpc_id" {
  filter {
    name   = "tag:Name"
    values = ["FinalChallengeVPC"]
  }
}

data "aws_subnets" "public_subnet" {
  filter {
    name   = "tag:Name"
    values = ["wp_public_subnet"]
  }
}

data "aws_subnets" "private_subnet" {
  filter {
    name   = "tag:Name"
    values = ["wp_private_subnet"]
  }
}

data "aws_subnets" "secure_subnet" {
  filter {
    name   = "tag:Name"
    values = ["wp_secure_subnet"]
  }
}

data "aws_subnet" "secure_subnet" {
  vpc_id     = data.aws_vpc.vpc_id.id
  cidr_block = "10.0.4.0/24"
  filter {
    name   = "tag:Name"
    values = ["wp_secure_subnet"]
  }
}

data "aws_subnet" "secure_subnet2" {
  vpc_id     = data.aws_vpc.vpc_id.id
  cidr_block = "10.0.5.0/24"
  filter {
    name   = "tag:Name"
    values = ["wp_secure_subnet"]
  }
}
# data "aws_ecs_cluster" "my-ecs-fargate" {
#   cluster_name = "my-ecs-fargate"
# }