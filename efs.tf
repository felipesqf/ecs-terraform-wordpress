resource "aws_efs_file_system" "efs-ecs" {
  # availability_zone_name = ["ap-southeast-2a", "ap-southeast-2b"]
  creation_token                  = "efsFinalTask"
  encrypted                       = true
  kms_key_id                      = data.aws_kms_key.kms_efs.arn
  throughput_mode                 = "bursting"
  provisioned_throughput_in_mibps = 0
  tags = {
    Name = "efsFinalTask"
  }
}

resource "aws_efs_mount_target" "mt-ecs" {
  count           = 1
  file_system_id  = aws_efs_file_system.efs-ecs.id
  subnet_id       = data.aws_subnet.secure_subnet.id
  security_groups = [aws_security_group.sg-wp-efs.id]
}

resource "aws_efs_mount_target" "mt-ecs2" {
  count           = 1
  file_system_id  = aws_efs_file_system.efs-ecs.id
  subnet_id       = data.aws_subnet.secure_subnet2.id
  security_groups = [aws_security_group.sg-wp-efs.id]
}

resource "aws_efs_access_point" "ap-ecs" {
  file_system_id = aws_efs_file_system.efs-ecs.id
  tags = {
    Name = "efsFinalTask"
  }
}

