
resource "aws_db_subnet_group" "rds_subnet_group" {
  name       = "aws_db_subnet_group"
  subnet_ids = data.aws_subnets.secure_subnet.ids
}

resource "aws_db_instance" "myrds" {
  identifier             = "wordpressdb"
  allocated_storage      = 10
  engine                 = "mysql"
  engine_version         = "8.0.34"
  instance_class         = "db.t2.micro"
  username               = var.db_username
  password               = random_string.password.result
  parameter_group_name   = "default.mysql8.0"
  port                   = "3306"
  vpc_security_group_ids = [aws_security_group.sg-wp-rds.id]
  db_subnet_group_name   = aws_db_subnet_group.rds_subnet_group.id
  skip_final_snapshot    = true
  multi_az               = false
}